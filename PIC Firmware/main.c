// IO Expander for 3D Printer Dual Extruders
//This project is susteined by CNC Brasil http://www.cncbrasil.ind.br/
// Project licensed under creative commons license BY SA
//------------------------------------------------------------------------------
#include <xc.h>
#include  <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <plib.h>
#include <timers.h>
#define PIC18F2520
#define USE_AND_MASKS
#include <pic18f2520.h>

// PIC18F2520 Configuration Bit Settings
#include <xc.h>
#include <math.h>
// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// CONFIG1H
#pragma config OSC = INTIO67    // Oscillator Selection bits (Internal oscillator block, port function on RA6 and RA7)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor enabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRT = ON        // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown Out Reset Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = ON         // Watchdog Timer Enable bit (WDT enabled)
#pragma config WDTPS = 8192     // Watchdog Timer Postscale Select bits (1:8192)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
#pragma config PBADEN = OFF     // PORTB A/D Enable bit (PORTB<4:0> pins are configured as digital I/O on Reset)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RE3 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = ON         // Single-Supply ICSP Enable bit (Single-Supply ICSP enabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit (Block 0 (000800-001FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit (Block 1 (002000-003FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit (Block 2 (004000-005FFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit (Block 3 (006000-007FFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit (Block 0 (000800-001FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit (Block 1 (002000-003FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit (Block 2 (004000-005FFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit (Block 3 (006000-007FFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit (Block 0 (000800-001FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit (Block 1 (002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit (Block 2 (004000-005FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit (Block 3 (006000-007FFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot block (000000-0007FFh) not protected from table reads executed in other blocks)

//Definitions ------------------------------------------------------------------
#define _XTAL_FREQ 8000000 // 8MHZ internal
#define FREQ_BASE   15000 //frequencia de base em Hz
#define RELOAD_VAL  (_XTAL_FREQ /2/ FREQ_BASE) //valor de recarga do TIMER0
#define E_STOP_X    PORTAbits.RA3
#define E_STOP_Y    PORTAbits.RA4
#define E_STOP_Z    PORTAbits.RA5
#define LED_R       PORTBbits.RB0
#define LED_G       PORTBbits.RB1
#define LED_B       PORTBbits.RB2
#define TEMP1       PORTAbits.RA0 
#define TEMP2       PORTAbits.RA1
#define TEMP3       PORTAbits.RA2
#define HEATER_1    PORTBbits.RB6
#define HEATER_2    PORTBbits.RB7
#define UART_RX     PORTCbits.RC7
#define UART_TX     PORTCbits.RC6
#define SERVO_1     PORTCbits.RC1
#define SERVO_2     PORTCbits.RC2
#define FAN_1       PORTAbits.RA6
#define FAN_2       PORTAbits.RA7
#define FAN_3       PORTCbits.RC0
#define AUX_1       PORTBbits.RB3
#define AUX_2       PORTBbits.RB4
#define AUX_3       PORTBbits.RB5
//Variables --------------------------------------------------------------------
static unsigned  char UART1Config =0, baund =0, ADCStringVal[4];
static char sPWM1 =0,sPWM2 =0,sPWM3 =0,sPWM4 =0, sPWM5 =0,sPWM6 =0,sPWM7 =0, sPWM8=0, CounterSPWM=0;
static char c;
static char dt;
static char i[4];
static float R =0, G=0, B=0;
static int wave =380;
//Pid's
static const float PID1_P=1, PID1_I = 0.1 , PID1_D= 0;
static const float PID2_P=1, PID2_I = 0.1 , PID2_D= 0;
static float  pid1_p =0, pid1_d =0, pid1_i=0;
static float  pid2_p =0, pid2_d =0, pid2_i=0;
static float pid1_current =0, pid2_current =0;
static float pid1_setpoint =0, pid2_setpoint =0;
static float pid1_lasterror =0, pid2_lasterror =0;
static float pid1_errorintegral =0, pid2_errorintegral =0;
static int tmr =0;
static char msb =0 , lsb =0;
//------------------------------------------------------------------------------
void getStreamFromFloat(float f)
{
    int i = (int) f;
    lsb =(char) (i & 0xFF);
    msb = (char) ((i > 8) & 0x7F );
}
void getStreamFromInt(int i)
{
    lsb =(char) (i & 0xFF);
    msb = (char) ((i > 8) & 0x7F );
}
float getTemp(char ch)
{
      SelChanConvADC(ch);
      ConvertADC();
      return (float)((ReadADC() * 0.048828125));
}
void main(void)
{
    //Configure oscillator for 8mhz
    OSCCONbits.IRCF0 = 1;
    OSCCONbits.IRCF1 = 1;
    OSCCONbits.IRCF2 = 1;
    OSCCONbits.SCS0 =1;
    OSCCONbits.SCS1 =1;
    while(!OSCCONbits.IOFS);
    //Configure PLL
    OSCTUNEbits.INTSRC = 1;
    OSCTUNEbits.PLLEN = 1;
    //Configure PORT'S and PIN'S

    TRISAbits.RA0 = 1; // TEMP 1
    TRISAbits.RA1 = 1; // TEMP 2
    TRISAbits.RA2 = 1; // TEMP 3
    TRISAbits.RA3 = 1; // E-STOP X
    TRISAbits.RA4 = 1; // E-STOP Y
    TRISAbits.RA5 = 1; // E-STOP Z
    TRISAbits.RA6 = 0; // FAN 1
    TRISAbits.RA7 = 0; // FAN 2

    TRISBbits.RB0 = 0; // LED R
    TRISBbits.RB1 = 0; // LED G
    TRISBbits.RB2 = 0; // LED B
    TRISBbits.RB3 = 1; // AUX1
    TRISBbits.RB4 = 0; // AUX2
    TRISBbits.RB5 = 0; // AUX3
    TRISBbits.RB6 = 0; // PGC HEATER 1
    TRISBbits.RB7 = 0; // PGD HEATER 2

    TRISCbits.RC0 = 0; // FAN3
    TRISCbits.RC1 = 0; // SERVO 1
    TRISCbits.RC2 = 0; // SERVO 2
    TRISCbits.RC3 = 0; // SPI CLK
    TRISCbits.RC4 = 1; // SPI SDI / MISO
    TRISCbits.RC5 = 0; // SPI MOSI (not used)
    TRISCbits.RC6 = 0; // UART TX
    TRISCbits.RC7 = 1; // UART RX

    //UART Config
    UART1Config = USART_TX_INT_OFF & USART_RX_INT_ON & USART_ASYNCH_MODE &
            USART_EIGHT_BIT & USART_BRGH_HIGH;
    baund = 51;
    OpenUSART(UART1Config,baund);
    //ADC Config
    OpenADC(ADC_FOSC_2 & ADC_RIGHT_JUST & ADC_20_TAD,
           ADC_CH0  & ADC_CH1  &ADC_CH2  &ADC_CH3  & ADC_INT_OFF
            & ADC_VREFPLUS_VDD & ADC_VREFMINUS_VSS,
            ADC_0ANA);
    //Timer Config
   // T0CON = 0x00;
   // T0CONbits.T08BIT = 1;
   // T0CONbits.T0CS = 0;
   // T0CONbits.PSA = 1;
   // TMR0  = RELOAD_VAL;
  //  T0CONbits.TMR0ON = 1;
    OpenTimer2(T2_PS_1_16 & T2_POST_1_2 &  TIMER_INT_OFF); // Hardware PWM
    //PWM Config
    OpenPWM1(0xFF);
    OpenPWM2(0xFF);
    //Enable Interrupts
   // INTCONbits.TMR0IF = 0;
   // INTCONbits.T0IE =1;
    PEIE =1; // Enable Periferall interrupts
    GIE =1; // Enable Global interrups
    ei();  // Enagle Interrupts
    LED_R =0;
    LED_G =0;
    LED_B =0;
    FAN_1 =0;
    FAN_2 =0;
    FAN_3 =0;
    HEATER_1 =0;
    HEATER_2 =0;
    CounterSPWM =0;
    while(1)
    {
        //These software pwm's dont have a critical frequency, so it can run  in main loop
        //and wait interrupt requests.
        //soft pwm
        if(sPWM1 > CounterSPWM)
            HEATER_1 = 1;
        else
            HEATER_1 =0;
        if(sPWM2 > CounterSPWM)
            HEATER_2 = 1;
        else
            HEATER_2 =0;
        if(sPWM3 >CounterSPWM)
            FAN_1 = 1;
        else
            FAN_1 =0;
        if(sPWM4 >CounterSPWM)
            FAN_2 = 1;
        else
            FAN_2 =0;
        if(sPWM5 >CounterSPWM)
            FAN_3 = 1;
        else
            FAN_3 =0;
        if(sPWM6 >CounterSPWM)
            LED_R = 1;
        else
            LED_R =0;
        if(sPWM7 >CounterSPWM)
            LED_G = 1;
        else
            LED_G =0;
        if(sPWM8 >CounterSPWM)
            LED_B = 1;
        else
            LED_B =0;
        //count from 0 to 255
        if(CounterSPWM ==255)
            CounterSPWM =0;
        else
            CounterSPWM++;
        CLRWDT();
    }
    //Calculate pid's
    if(tmr >= 512)
    {
        float derivative;
        float power;
        pid1_current =getTemp(1);
        pid2_current =getTemp(2);
        float error1 = pid1_setpoint - pid1_current;
        float error2 = pid1_setpoint - pid1_current;
        if(abs((int)error1) > 15)
            if(error1 > 0)
                sPWM1 = 255;
            else
                sPWM1 = 0;
        else
        {
            //get derivative
            derivative = error1 - pid1_lasterror;
            pid1_lasterror = error1;
            //get integral
            pid1_errorintegral += error1;
            //process pid
            power = (PID1_P * error1) + (PID1_D * derivative) + (PID1_I * pid1_errorintegral);
            power = max(min(power,1.0),0.0);
            if(pid1_setpoint ==0 || pid1_current < 5 || pid1_current > 400)
                power =0;
            sPWM1 =(int)( power * 255);
        }
        if(abs((int)error2) > 15)
            if(error2 > 0)
                sPWM2 = 255;
            else
                sPWM2 = 0;
        else
        {
            //get derivative
            derivative = error2 - pid2_lasterror;
            pid2_lasterror = error2;
            //get integral
            pid2_errorintegral += error2;
            //process pid
            power = (PID2_P * error1) + (PID2_D * derivative) + (PID2_I * pid1_errorintegral);
            power = max(min(power,1.0),0.0);
            if(pid2_setpoint ==0 || pid2_current < 5 || pid2_current > 400)
                power =0;
            sPWM2 =(int) (power * 255);
        }
        tmr = 0;
    }
    else
        tmr++;
}
//Interrupts -------------------------------------------------------------------
void interrupt Timer1Interrupt(void)
{
    if(PIR1bits.RCIF)
    {
        c =0;

        c = getcUSART();
        switch(c)
        {
            case 'E':
                c = getcUSART();
                if(c == 'X')
                {
                    if(E_STOP_X)
                    {
                        putcUSART('X');
                        putcUSART('1');
                    }
                    else
                    {
                        putcUSART('X');
                        putcUSART('0');
                    }

                }
                if(c == 'Y')
                {
                    if(E_STOP_Y)
                    {
                        putcUSART('Y');
                        putcUSART('1');
                    }
                    else
                    {
                        putcUSART('Y');
                        putcUSART('0');
                    }
                }
                if(c=='Z')
                {
                    if(E_STOP_Z)
                    {
                        putcUSART('Z');
                        putcUSART('1');
                    }
                    else
                    {
                        putcUSART('Z');
                        putcUSART('0');
                    }
                }
                break;
            case 'T':
                c = getcUSART();
                if(c == '1')
                {
                    putcUSART('O');
                    putcUSART('K');
                    putcUSART('T');
                    putcUSART('1');
                    getStreamFromFloat(pid1_current);
                    putcUSART(lsb);
                    putcUSART(msb);
                }
                if(c == '2')
                {
                    putcUSART('O');
                    putcUSART('K');
                    putcUSART('T');
                    putcUSART('2');
                    getStreamFromFloat(pid2_current);
                    putcUSART(lsb);
                    putcUSART(msb);
                }
                break;
            case 'A':
                //Get ADC Channel
                c = getcUSART();
                SelChanConvADC((unsigned char)c);
                ConvertADC();
                putcUSART('O');
                putcUSART('K');
                putcUSART('A');
                putcUSART(c);
                getStreamFromInt(ReadADC());
                putcUSART(lsb);
                putcUSART(msb);
                break;
            case 'P': //Set PWM Channel
               //get ch
                c = getcUSART();
                dt = getcUSART();
                switch(c)
                {
                    case 1: // SERVO 1
                        //Hardware PWM
                        SetDCPWM1(dt *4);
                        break;
                    case 2: // SERVO 2
                        //Hardware PWM
                        SetDCPWM2(dt *4);
                        break;
                    case 3: // HEATER 1
                        //Software PWM
                        sPWM1 = dt;
                        break;
                    case 4: // HEATER 2
                        //Software PWM
                        sPWM2 = dt;
                        break;
                    case 5: // FAN 1
                        //Software PWM
                        sPWM3 =dt;
                        break;
                    case 6: // FAN 2
                        //Software PWM
                        sPWM4 = dt;
                        break;
                    case 7: // FAN 3
                        //Software PWM
                        sPWM5 = dt;
                        break;
                    case 8: // FAN 3
                        //Software PWM
                        sPWM6 = dt;
                        break;
                    case 9: // FAN 3
                        //Software PWM
                        sPWM7 =dt;
                        break;
                    case 10: // FAN 3
                        //Software PWM
                        sPWM8 = dt;
                        break;
                }
                break;
            case 'R': // Firmware Reset
                Reset();
                break;
        }
        //clear flag
        PIR1bits.RCIF = 0;
       //WriteTimer0(0x14);
    }
}

//------------------------------------------------------------------------------
