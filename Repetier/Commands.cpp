#include "Repetier.h"

const int sensitive_pins[] PROGMEM = SENSITIVE_PINS; // Sensitive pin list for M42
int Commands::lowestRAMValue = MAX_RAM;
int Commands::lowestRAMValueSend = MAX_RAM;

void Commands::commandLoop()
{
    while(true)
    {
        GCode::readFromSerial();
        GCode *code = GCode::peekCurrentCommand();
        //UI_SLOW; // do longer timed user interface action
        UI_MEDIUM; // do check encoder
        if(code)
        {
            Commands::executeGCode(code);
            code->popCurrentCommand();
        }
        Printer::defaultLoopActions();
    }
}

void Commands::checkForPeriodicalActions(bool allowNewMoves)
{
    if(!executePeriodical) return;
    executePeriodical = 0;
    Extruder::manageTemperatures();
    if(--counter250ms == 0)
    {
        if(manageMonitor <= 1 + NUM_EXTRUDER)
            writeMonitor();
        counter250ms = 5;
    }
    // If called from queueDelta etc. it is an error to start a new move since it
    // would invalidate old computation resulting in unpredicted behaviour.
    // lcd controller can start new moves, so we disallow it if called from within
    // a move command.
    UI_SLOW(allowNewMoves);
}

/** \brief Waits until movement cache is empty.

  Some commands expect no movement, before they can execute. This function
  waits, until the steppers are stopped. In the meanwhile it buffers incoming
  commands and manages temperatures.
*/
void Commands::waitUntilEndOfAllMoves()
{
    while(PrintLine::hasLines())
    {
        GCode::readFromSerial();
        checkForPeriodicalActions(false);
        UI_MEDIUM;
    }
}

void Commands::waitUntilEndOfAllBuffers()
{
    GCode *code = NULL;
    while(PrintLine::hasLines() || (code != NULL))
    {
        GCode::readFromSerial();
        code = GCode::peekCurrentCommand();
        UI_MEDIUM; // do check encoder
        if(code)
        {
            Commands::executeGCode(code);
            code->popCurrentCommand();
        }
        Commands::checkForPeriodicalActions(false); // only called from memory
        UI_MEDIUM;
    }
}

void Commands::printCurrentPosition(FSTRINGPARAM(s))
{
    float x, y, z;
    Printer::realPosition(x, y, z);
    if (isnan(x) || isinf(x) || isnan(y) || isinf(y) || isnan(z) || isinf(z))
    {
        Com::printErrorFLN(s); // flag where the error condition came from
    }
    x += Printer::coordinateOffset[X_AXIS];
    y += Printer::coordinateOffset[Y_AXIS];
    z += Printer::coordinateOffset[Z_AXIS];
    Com::printF(Com::tXColon, x * (Printer::unitIsInches ? 0.03937 : 1), 2);
    Com::printF(Com::tSpaceYColon, y * (Printer::unitIsInches ? 0.03937 : 1), 2);
    Com::printF(Com::tSpaceZColon, z * (Printer::unitIsInches ? 0.03937 : 1), 3);
    Com::printFLN(Com::tSpaceEColon, Printer::currentPositionSteps[E_AXIS] * Printer::invAxisStepsPerMM[E_AXIS] * (Printer::unitIsInches ? 0.03937 : 1), 4);;
}

void Commands::printTemperatures(bool showRaw)
{
	Com::printF(Com::tTColon, IOExpanderClass::GetTemp(0));
    Com::printF(Com::tSpaceSlash,IOExpander.GetTemp(IOExpander.GetActiveExtruder()),0);
    Com::printF(Com::tSpaceBColon,Extruder::getHeatedBedTemperature());
    Com::printF(Com::tSpaceSlash,heatedBedController.targetTemperatureC,0);
   // if(showRaw)
   // {
   //     Com::printF(Com::tSpaceRaw,(int)NUM_EXTRUDER);
   //     Com::printF(Com::tColon,(1023<<(2-ANALOG_REDUCE_BITS))-heatedBedController.currentTemperature);
   // }
    //Com::printF(Com::tSpaceBAtColon,(pwm_pos[heatedBedController.pwmIndex])); // Show output of autotune when tuning!

	//IOEXPANDER only permits two extruders
	Com::printF(Com::tSpaceT,0);
	Com::printF(Com::tColon, IOExpanderClass::GetTemp(0));
	Com::printF(Com::tSpaceSlash, IOExpanderClass::GetSetPoint(0), 0);

	Com::printF(Com::tSpaceT,1);
	Com::printF(Com::tColon, IOExpanderClass::GetTemp(1));
	Com::printF(Com::tSpaceSlash,IOExpanderClass::GetSetPoint(1), 0);

    Com::println();
}
void Commands::changeFeedrateMultiply(int factor)
{
    if(factor < 25) factor = 25;
    if(factor > 500) factor = 500;
    Printer::feedrate *= (float)factor / (float)Printer::feedrateMultiply;
    Printer::feedrateMultiply = factor;
    Com::printFLN(Com::tSpeedMultiply, factor);
}

void Commands::setFanSpeed(int speed,bool wait)
{
#if FAN_PIN>-1 && FEATURE_FAN_CONTROL
    speed = constrain(speed,0,255);
    Printer::setMenuMode(MENU_MODE_FAN_RUNNING,speed != 0);
    if(wait)
        Commands::waitUntilEndOfAllMoves(); // use only if neededthis to change the speed exactly at that point, but it may cause blobs if you do!
    if(speed != pwm_pos[NUM_EXTRUDER + 2])
        Com::printFLN(Com::tFanspeed,speed); // send only new values to break update loops!
    pwm_pos[NUM_EXTRUDER + 2] = speed;
#endif
}

/**
  \brief Execute the Arc command stored in com.
*/
#if ARC_SUPPORT
void Commands::processArc(GCode *com)
{
    float position[Z_AXIS_ARRAY];
    Printer::realPosition(position[X_AXIS],position[Y_AXIS],position[Z_AXIS]);
    if(!Printer::setDestinationStepsFromGCode(com)) return; // For X Y Z E F
    float offset[2] = {Printer::convertToMM(com->hasI() ? com->I : 0),Printer::convertToMM(com->hasJ() ? com->J : 0)};
    float target[E_AXIS_ARRAY] = {Printer::realXPosition(),Printer::realYPosition(),Printer::realZPosition(),Printer::destinationSteps[E_AXIS]*Printer::invAxisStepsPerMM[E_AXIS]};
    float r;
    if (com->hasR())
    {
        /*
          We need to calculate the center of the circle that has the designated radius and passes
          through both the current position and the target position. This method calculates the following
          set of equations where [x,y] is the vector from current to target position, d == magnitude of
          that vector, h == hypotenuse of the triangle formed by the radius of the circle, the distance to
          the center of the travel vector. A vector perpendicular to the travel vector [-y,x] is scaled to the
          length of h [-y/d*h, x/d*h] and added to the center of the travel vector [x/2,y/2] to form the new point
          [i,j] at [x/2-y/d*h, y/2+x/d*h] which will be the center of our arc.

          d^2 == x^2 + y^2
          h^2 == r^2 - (d/2)^2
          i == x/2 - y/d*h
          j == y/2 + x/d*h

                                                               O <- [i,j]
                                                            -  |
                                                  r      -     |
                                                      -        |
                                                   -           | h
                                                -              |
                                  [0,0] ->  C -----------------+--------------- T  <- [x,y]
                                            | <------ d/2 ---->|

          C - Current position
          T - Target position
          O - center of circle that pass through both C and T
          d - distance from C to T
          r - designated radius
          h - distance from center of CT to O

          Expanding the equations:

          d -> sqrt(x^2 + y^2)
          h -> sqrt(4 * r^2 - x^2 - y^2)/2
          i -> (x - (y * sqrt(4 * r^2 - x^2 - y^2)) / sqrt(x^2 + y^2)) / 2
          j -> (y + (x * sqrt(4 * r^2 - x^2 - y^2)) / sqrt(x^2 + y^2)) / 2

          Which can be written:

          i -> (x - (y * sqrt(4 * r^2 - x^2 - y^2))/sqrt(x^2 + y^2))/2
          j -> (y + (x * sqrt(4 * r^2 - x^2 - y^2))/sqrt(x^2 + y^2))/2

          Which we for size and speed reasons optimize to:

          h_x2_div_d = sqrt(4 * r^2 - x^2 - y^2)/sqrt(x^2 + y^2)
          i = (x - (y * h_x2_div_d))/2
          j = (y + (x * h_x2_div_d))/2

        */
        r = Printer::convertToMM(com->R);
        // Calculate the change in position along each selected axis
        double x = target[X_AXIS]-position[X_AXIS];
        double y = target[Y_AXIS]-position[Y_AXIS];

        double h_x2_div_d = -sqrt(4 * r*r - x*x - y*y)/hypot(x,y); // == -(h * 2 / d)
        // If r is smaller than d, the arc is now traversing the complex plane beyond the reach of any
        // real CNC, and thus - for practical reasons - we will terminate promptly:
        if(isnan(h_x2_div_d))
        {
            Com::printErrorFLN(Com::tInvalidArc);
            return;
        }
        // Invert the sign of h_x2_div_d if the circle is counter clockwise (see sketch below)
        if (com->G==3)
        {
            h_x2_div_d = -h_x2_div_d;
        }

        /* The counter clockwise circle lies to the left of the target direction. When offset is positive,
           the left hand circle will be generated - when it is negative the right hand circle is generated.


                                                         T  <-- Target position

                                                         ^
              Clockwise circles with this center         |          Clockwise circles with this center will have
              will have > 180 deg of angular travel      |          < 180 deg of angular travel, which is a good thing!
                                               \         |          /
        center of arc when h_x2_div_d is positive ->  x <----- | -----> x <- center of arc when h_x2_div_d is negative
                                                         |
                                                         |

                                                         C  <-- Current position                                 */


        // Negative R is g-code-alese for "I want a circle with more than 180 degrees of travel" (go figure!),
        // even though it is advised against ever generating such circles in a single line of g-code. By
        // inverting the sign of h_x2_div_d the center of the circles is placed on the opposite side of the line of
        // travel and thus we get the unadvisably long arcs as prescribed.
        if (r < 0)
        {
            h_x2_div_d = -h_x2_div_d;
            r = -r; // Finished with r. Set to positive for mc_arc
        }
        // Complete the operation by calculating the actual center of the arc
        offset[0] = 0.5*(x-(y*h_x2_div_d));
        offset[1] = 0.5*(y+(x*h_x2_div_d));

    }
    else     // Offset mode specific computations
    {
        r = hypot(offset[0], offset[1]); // Compute arc radius for arc
    }
    // Set clockwise/counter-clockwise sign for arc computations
    uint8_t isclockwise = com->G == 2;
    // Trace the arc
    PrintLine::arc(position, target, offset, r, isclockwise);
}
#endif

/**
  \brief Execute the G command stored in com.
*/
void Commands::processGCode(GCode *com)
{
    uint32_t codenum; //throw away variable
    switch(com->G)
    {
    case 0: // G0 -> G1
    case 1: // G1
        if(com->hasS()) Printer::setNoDestinationCheck(com->S != 0);
        if(Printer::setDestinationStepsFromGCode(com)) // For X Y Z E F

        PrintLine::queueCartesianMove(ALWAYS_CHECK_ENDSTOPS, true);
        break;
#if ARC_SUPPORT
    case 2: // CW Arc
    case 3: // CCW Arc MOTION_MODE_CW_ARC: case MOTION_MODE_CCW_ARC:
        processArc(com);
        break;
#endif
    case 4: // G4 dwell
        Commands::waitUntilEndOfAllMoves();
        codenum = 0;
        if(com->hasP()) codenum = com->P; // milliseconds to wait
        if(com->hasS()) codenum = com->S * 1000; // seconds to wait
        codenum += HAL::timeInMilliseconds();  // keep track of when we started waiting
        while((uint32_t)(codenum-HAL::timeInMilliseconds())  < 2000000000 )
        {
            GCode::readFromSerial();
            Commands::checkForPeriodicalActions(true);
        }
        break;
    case 20: // G20 Units to inches
        Printer::unitIsInches = 1;
        break;
    case 21: // G21 Units to mm
        Printer::unitIsInches = 0;
        break;
    case 28:  //G28 Home all Axis one at a time
    {

        uint8_t homeAllAxis = (com->hasNoXYZ() && !com->hasE());
        if(homeAllAxis || !com->hasNoXYZ())
            Printer::homeAxis(homeAllAxis || com->hasX(),homeAllAxis || com->hasY(),homeAllAxis || com->hasZ());
		if (com->hasE())
			Printer::currentPositionSteps[E_AXIS] = 0;
		Printer::updateCurrentPosition();
    }
    break;
    case 29: // G29 3 points, build average or distortion compensation
    {
        GCode::executeFString(Com::tZProbeStartScript);
        bool oldAutolevel = Printer::isAutolevelActive();
        Printer::setAutolevelActive(false);
        float sum = 0, last,oldFeedrate = Printer::feedrate;
        Printer::moveTo(EEPROM::zProbeX1(),EEPROM::zProbeY1(),IGNORE_COORDINATE,IGNORE_COORDINATE,EEPROM::zProbeXYSpeed());
        sum = Printer::runZProbe(true,false,Z_PROBE_REPETITIONS,false);
        if(sum < 0) break;
        Printer::moveTo(EEPROM::zProbeX2(),EEPROM::zProbeY2(),IGNORE_COORDINATE,IGNORE_COORDINATE,EEPROM::zProbeXYSpeed());
        last = Printer::runZProbe(false,false);
        if(last < 0) break;
        sum+= last;
        Printer::moveTo(EEPROM::zProbeX3(),EEPROM::zProbeY3(),IGNORE_COORDINATE,IGNORE_COORDINATE,EEPROM::zProbeXYSpeed());
        last = Printer::runZProbe(false,true);
        if(last < 0) break;
        sum += last;
        sum *= 0.33333333333333;
        Com::printFLN(Com::tZProbeAverage,sum);
        if(com->hasS() && com->S)
        {

            Printer::currentPositionSteps[Z_AXIS] = sum * Printer::axisStepsPerMM[Z_AXIS];
            Com::printFLN(PSTR("Adjusted z origin"));
        }
        Printer::feedrate = oldFeedrate;
        Printer::setAutolevelActive(oldAutolevel);
        if(com->hasS() && com->S == 2)
            EEPROM::storeDataIntoEEPROM();
        Printer::updateCurrentPosition(true);
        printCurrentPosition(PSTR("G29 "));
        GCode::executeFString(Com::tZProbeEndScript);
        Printer::feedrate = oldFeedrate;
    }
    break;
    case 30: // G30 single probe set Z0
    {
        uint8_t p = (com->hasP() ? (uint8_t)com->P : 3);
        Printer::runZProbe(p & 1,p & 2);
        Printer::updateCurrentPosition(p & 1);
    }
    break;
    case 31:  // G31 display hall sensor output
        Com::printF(Com::tZProbeState);
        Com::print(Printer::isZProbeHit() ? 'H' : 'L');
        Com::println();
        break;
    case 32: // G32 Auto-Bed leveling
    {
        GCode::executeFString(Com::tZProbeStartScript);
        //bool iterate = com->hasP() && com->P>0;
        Printer::coordinateOffset[X_AXIS] = Printer::coordinateOffset[Y_AXIS] = Printer::coordinateOffset[Z_AXIS] = 0;
        Printer::setAutolevelActive(false); // iterate
        float h1,h2,h3,hc,oldFeedrate = Printer::feedrate;
        Printer::moveTo(EEPROM::zProbeX1(),EEPROM::zProbeY1(),IGNORE_COORDINATE,IGNORE_COORDINATE,EEPROM::zProbeXYSpeed());
        h1 = Printer::runZProbe(true,false,Z_PROBE_REPETITIONS,false);
        if(h1 < 0) break;
        Printer::moveTo(EEPROM::zProbeX2(),EEPROM::zProbeY2(),IGNORE_COORDINATE,IGNORE_COORDINATE,EEPROM::zProbeXYSpeed());
        h2 = Printer::runZProbe(false,false);
        if(h2 < 0) break;
        Printer::moveTo(EEPROM::zProbeX3(),EEPROM::zProbeY3(),IGNORE_COORDINATE,IGNORE_COORDINATE,EEPROM::zProbeXYSpeed());
        h3 = Printer::runZProbe(false,true);
        if(h3 < 0) break;
        Printer::buildTransformationMatrix(h1,h2,h3);
        //-(Rxx*Ryz*y-Rxz*Ryx*y+(Rxz*Ryy-Rxy*Ryz)*x)/(Rxy*Ryx-Rxx*Ryy)
        // z = z-deviation from origin due to bed transformation
        float z = -((Printer::autolevelTransformation[0] * Printer::autolevelTransformation[5] -
                     Printer::autolevelTransformation[2] * Printer::autolevelTransformation[3]) *
                    (float)Printer::currentPositionSteps[Y_AXIS] * Printer::invAxisStepsPerMM[Y_AXIS] +
                    (Printer::autolevelTransformation[2] * Printer::autolevelTransformation[4] -
                     Printer::autolevelTransformation[1] * Printer::autolevelTransformation[5]) *
                    (float)Printer::currentPositionSteps[X_AXIS] * Printer::invAxisStepsPerMM[X_AXIS]) /
                  (Printer::autolevelTransformation[1] * Printer::autolevelTransformation[3] - Printer::autolevelTransformation[0] * Printer::autolevelTransformation[4]);
        Printer::zMin = 0;
        if(com->hasS() && com->S < 3 && com->S > 0)
        {

            Printer::currentPositionSteps[Z_AXIS] = (h3 + z) * Printer::axisStepsPerMM[Z_AXIS];
            Printer::setAutolevelActive(true);
            if(com->S == 2)
                EEPROM::storeDataIntoEEPROM();
        }
        else
        {
            Printer::currentPositionSteps[Z_AXIS] = (h3 + z) * Printer::axisStepsPerMM[Z_AXIS];
            if(com->hasS() && com->S == 3)
                EEPROM::storeDataIntoEEPROM();
        }
        Printer::setAutolevelActive(true);
        Printer::updateDerivedParameter();
        Printer::updateCurrentPosition(true);
        printCurrentPosition(PSTR("G32 "));
        Printer::feedrate = oldFeedrate;
    }
    break;
    case 90: // G90
        Printer::relativeCoordinateMode = false;
        if(com->internalCommand)
            Com::printInfoFLN(PSTR("Absolute positioning"));
        break;
    case 91: // G91
        Printer::relativeCoordinateMode = true;
        if(com->internalCommand)
            Com::printInfoFLN(PSTR("Relative positioning"));
        break;
    case 92: // G92
    {
        float xOff = Printer::coordinateOffset[X_AXIS];
        float yOff = Printer::coordinateOffset[Y_AXIS];
        float zOff = Printer::coordinateOffset[Z_AXIS];
        if(com->hasX()) xOff = Printer::convertToMM(com->X) - Printer::currentPosition[X_AXIS];
        if(com->hasY()) yOff = Printer::convertToMM(com->Y) - Printer::currentPosition[Y_AXIS];
        if(com->hasZ()) zOff = Printer::convertToMM(com->Z) - Printer::currentPosition[Z_AXIS];
        Printer::setOrigin(xOff, yOff, zOff);
        if(com->hasE())
        {
            Printer::currentPositionSteps[E_AXIS] = Printer::convertToMM(com->E) * Printer::axisStepsPerMM[E_AXIS];
        }
    }
    break;
    default:
        if(Printer::debugErrors())
        {
            Com::printF(Com::tUnknownCommand);
            com->printCommand();
        }
    }
    previousMillisCmd = HAL::timeInMilliseconds();
}
/**
  \brief Execute the G command stored in com.
*/
void Commands::processMCode(GCode *com)
{
    uint32_t codenum; //throw away variable
    switch( com->M )
    {

    case 42: //M42 -Change pin status via gcode
        if (com->hasP())
        {
            int pin_number = com->P;
            for(uint8_t i = 0; i < (uint8_t)sizeof(sensitive_pins); i++)
            {
                if (pgm_read_byte(&sensitive_pins[i]) == pin_number)
                {
                    pin_number = -1;
                    break;
                }
            }
            if (pin_number > -1)
            {
                if(com->hasS()) {
                    if(com->S >= 0 && com->S <= 255) {
                        pinMode(pin_number, OUTPUT);
                        digitalWrite(pin_number, com->S);
                        analogWrite(pin_number, com->S);
                        Com::printF(Com::tSetOutputSpace, pin_number);
                        Com::printFLN(Com::tSpaceToSpace,(int)com->S);
                    } else
                        Com::printErrorFLN(PSTR("Illegal S value for M42"));
                } else {
					pinMode(pin_number, INPUT_PULLUP);
					Com::printF(Com::tSpaceToSpace, pin_number);
					Com::printFLN(Com::tSpaceIsSpace, digitalRead(pin_number));
                }
            } else {
                Com::printErrorFLN(PSTR("Pin can not be set by M42, may in invalid or in use. "));
            }
        }
        break;

    case 80: // M80 - ATX Power On
#if PS_ON_PIN>-1
        Commands::waitUntilEndOfAllMoves();
        previousMillisCmd = HAL::timeInMilliseconds();
        SET_OUTPUT(PS_ON_PIN); //GND
        Printer::setPowerOn(true);
        WRITE(PS_ON_PIN, (POWER_INVERTING ? HIGH : LOW));
#endif
        break;
    case 81: // M81 - ATX Power Off
#if PS_ON_PIN>-1
        Commands::waitUntilEndOfAllMoves();
        SET_OUTPUT(PS_ON_PIN); //GND
        Printer::setPowerOn(false);
        WRITE(PS_ON_PIN,(POWER_INVERTING ? LOW : HIGH));
#endif
        break;
    case 82: // M82
        Printer::relativeExtruderCoordinateMode = false;
        break;
    case 83: // M83
        Printer::relativeExtruderCoordinateMode = true;
        break;
    case 84: // M84
        if(com->hasS())
        {
            stepperInactiveTime = com->S * 1000;
        }
        else
        {
            Commands::waitUntilEndOfAllMoves();
            Printer::kill(true);
        }
        break;
    case 85: // M85
        if(com->hasS())
            maxInactiveTime = (int32_t)com->S * 1000;
        else
            maxInactiveTime = 0;
        break;
    case 92: // M92
        if(com->hasX()) Printer::axisStepsPerMM[X_AXIS] = com->X;
        if(com->hasY()) Printer::axisStepsPerMM[Y_AXIS] = com->Y;
        if(com->hasZ()) Printer::axisStepsPerMM[Z_AXIS] = com->Z;
        Printer::updateDerivedParameter();
        if(com->hasE())
        {
            Extruder::current->stepsPerMM = com->E;
            Extruder::selectExtruderById(Extruder::current->id);
        }
        break;
    case 99: // M99 S<time>
    {
        millis_t wait = 10000;
        if(com->hasS())
            wait = 1000*com->S;
        if(com->hasX())
            Printer::disableXStepper();
        if(com->hasY())
            Printer::disableYStepper();
        if(com->hasZ())
            Printer::disableZStepper();
        wait += HAL::timeInMilliseconds();
        while(wait-HAL::timeInMilliseconds() < 100000)
        {
            Printer::defaultLoopActions();
        }
        if(com->hasX())
            Printer::enableXStepper();
        if(com->hasY())
            Printer::enableYStepper();
        if(com->hasZ())
            Printer::enableZStepper();
    }
    break;

    case 104: // M104 temperature
		if(com->hasP() || (com->hasS() && com->S == 0))
			Commands::waitUntilEndOfAllMoves();
		if (com->hasS())
		{
			if (com->hasT())
			{
				IOExpanderClass::SetTemp((byte)com->T, (int)com->S);
			}
			else
			{
				IOExpanderClass::SetTemp((byte)Extruder::current->id, (int)com->S);
			}
		}
        break;
    case 140: // M140 set bed temp
        if(reportTempsensorError()) break;
        previousMillisCmd = HAL::timeInMilliseconds();
        if(Printer::debugDryrun()) break;
        if (com->hasS()) Extruder::setHeatedBedTemperature(com->S,com->hasF() && com->F>0);
        break;
    case 105: // M105  get temperature. Always returns the current temperature, doesn't wait until move stopped
        printTemperatures(com->hasX());
        break;
    case 109: // M109 - Wait for extruder heater to reach target.

		#if NUM_EXTRUDER>0
			{
				if(reportTempsensorError()) break;
				previousMillisCmd = HAL::timeInMilliseconds();
				if(Printer::debugDryrun()) break;
				UI_STATUS_UPD(UI_TEXT_HEATING_EXTRUDER);
				Commands::waitUntilEndOfAllMoves();
				Extruder *actExtruder = Extruder::current;
				if(com->hasT() && com->T<NUM_EXTRUDER) actExtruder = &extruder[com->T];
				if (com->hasT() && com->T<NUM_EXTRUDER) IOExpander.SetActiveExtruder( com->T);
				if (com->hasS()) Extruder::setTemperatureForExtruder(com->S,actExtruder->id,com->hasF() && com->F>0);
				if (com->hasS()) IOExpander.SetTemp(IOExpander.GetActiveExtruder(), com->S);
		#if defined(SKIP_M109_IF_WITHIN) && SKIP_M109_IF_WITHIN > 0
				if(abs(actExtruder->tempControl.currentTemperatureC - actExtruder->tempControl.targetTemperatureC)<(SKIP_M109_IF_WITHIN)) break; // Already in range
		#endif
				bool dirRising = actExtruder->tempControl.targetTemperature > actExtruder->tempControl.currentTemperature;
				millis_t printedTime = HAL::timeInMilliseconds();
				millis_t waituntil = 0;
		#if RETRACT_DURING_HEATUP
				uint8_t retracted = 0;
		#endif
				millis_t currentTime;
				do
				{
					previousMillisCmd = currentTime = HAL::timeInMilliseconds();
					if( (currentTime - printedTime) > 1000 )   //Print Temp Reading every 1 second while heating up.
					{
						printTemperatures();
						printedTime = currentTime;
					}
					Commands::checkForPeriodicalActions(true);
					//gcode_read_serial();
		#if RETRACT_DURING_HEATUP
					if (actExtruder == Extruder::current && actExtruder->waitRetractUnits > 0 && !retracted && dirRising && actExtruder->tempControl.currentTemperatureC > actExtruder->waitRetractTemperature)
					{
						PrintLine::moveRelativeDistanceInSteps(0,0,0,-actExtruder->waitRetractUnits * Printer::axisStepsPerMM[E_AXIS],actExtruder->maxFeedrate,false,false);
						retracted = 1;
					}
		#endif
					if((waituntil == 0 &&
							(dirRising ? actExtruder->tempControl.currentTemperatureC >= actExtruder->tempControl.targetTemperatureC - 1
							 : actExtruder->tempControl.currentTemperatureC <= actExtruder->tempControl.targetTemperatureC+1))
		#if defined(TEMP_HYSTERESIS) && TEMP_HYSTERESIS>=1
							|| (waituntil!=0 && (abs(actExtruder->tempControl.currentTemperatureC - actExtruder->tempControl.targetTemperatureC))>TEMP_HYSTERESIS)
		#endif
					  )
					{
						waituntil = currentTime+1000UL*(millis_t)actExtruder->watchPeriod; // now wait for temp. to stabalize
					}
				}
				while(waituntil==0 || (waituntil!=0 && (millis_t)(waituntil-currentTime)<2000000000UL));
		#if RETRACT_DURING_HEATUP
				if (retracted && actExtruder==Extruder::current)
				{
					PrintLine::moveRelativeDistanceInSteps(0,0,0,actExtruder->waitRetractUnits * Printer::axisStepsPerMM[E_AXIS],actExtruder->maxFeedrate,false,false);
				}
		#endif
			}
			UI_CLEAR_STATUS;
		#endif
			previousMillisCmd = HAL::timeInMilliseconds();
    break;
    case 190: // M190 - Wait bed for heater to reach target.
        if(Printer::debugDryrun()) break;
        UI_STATUS_UPD(UI_TEXT_HEATING_BED);
        Commands::waitUntilEndOfAllMoves();
        if (com->hasS()) Extruder::setHeatedBedTemperature(com->S,com->hasF() && com->F>0);
        if(abs(heatedBedController.currentTemperatureC-heatedBedController.targetTemperatureC)<SKIP_M190_IF_WITHIN) break;
        codenum = HAL::timeInMilliseconds();
        while(heatedBedController.currentTemperatureC + 0.5 < heatedBedController.targetTemperatureC)
        {
            if( (HAL::timeInMilliseconds()-codenum) > 1000 )   //Print Temp Reading every 1 second while heating up.
            {
                printTemperatures();
                codenum = previousMillisCmd = HAL::timeInMilliseconds();
            }
            Commands::checkForPeriodicalActions(true);
        }
        UI_CLEAR_STATUS;
        previousMillisCmd = HAL::timeInMilliseconds();
        break;
    case 116: // Wait for temperatures to reach target temperature
        if(Printer::debugDryrun()) break;
        {
            bool allReached = false;
            codenum = HAL::timeInMilliseconds();
            while(!allReached)
            {
                allReached = true;
                if( (HAL::timeInMilliseconds()-codenum) > 1000 )   //Print Temp Reading every 1 second while heating up.
                {
                    printTemperatures();
                    codenum = HAL::timeInMilliseconds();
                }
                Commands::checkForPeriodicalActions(true);

				TemperatureController *act = tempController[0];
				if(act->targetTemperatureC > 30 && fabs(act->targetTemperatureC-act->currentTemperatureC) > 1)
					allReached = false;
				if (IOExpanderClass::GetSetPoint(0) > 30 && fabs(IOExpanderClass::GetTemp(0)) > 1)
					allReached = false;
				if (IOExpanderClass::GetSetPoint(1) > 30 && fabs(IOExpanderClass::GetTemp(1)) > 1)
					allReached = false;

            }
        }
        break;
    case 106: // M106 Fan On
        //setFanSpeed(com->hasS()?com->S:255,com->hasP());
        break;
    case 107: // M107 Fan Off
       // setFanSpeed(0,com->hasP());
        break;
    case 115: // M115
        Com::printFLN(Com::tFirmware);
        break;
    case 114: // M114
        printCurrentPosition(PSTR("M114 "));
        break;
    case 119: // M119
        Commands::waitUntilEndOfAllMoves();
		Com::printF(Com::tXMinColon);
		Com::printF(Printer::isXMinEndstopHit()?Com::tHSpace:Com::tLSpace);
		Com::printF(Com::tYMinColon);
		Com::printF(Printer::isYMinEndstopHit()?Com::tHSpace:Com::tLSpace);
		Com::printF(Com::tZMinColon);
		Com::printF(Printer::isZMinEndstopHit()?Com::tHSpace:Com::tLSpace);
        Com::println();
        break;
    case 203: // M203 Temperature monitor
        if(com->hasS())
        {
            if(com->S<NUM_EXTRUDER) manageMonitor = com->S;
            else manageMonitor=NUM_EXTRUDER; // Set 100 to heated bed
        }
        break;
    case 204: // M204
    {
        TemperatureController *temp = &Extruder::current->tempControl;
        if(com->hasS())
        {
            if(com->S<0) break;
            if(com->S<NUM_EXTRUDER) temp = &extruder[com->S].tempControl;
            else temp = &heatedBedController;
        }
        if(com->hasX()) temp->pidPGain = com->X;
        if(com->hasY()) temp->pidIGain = com->Y;
        if(com->hasZ()) temp->pidDGain = com->Z;
        temp->updateTempControlVars();
    }
    break;

    case 220: // M220 S<Feedrate multiplier in percent>
        changeFeedrateMultiply(com->getS(100));
        break;

    case 320: // M320 Activate autolevel
        Printer::setAutolevelActive(true);
        if(com->hasS() && com->S)
        {
            EEPROM::storeDataIntoEEPROM();
        }
        break;
    case 321: // M321 Deactivate autoleveling
        Printer::setAutolevelActive(false);
        if(com->hasS() && com->S)
        {
            if(com->S == 3)
                Printer::resetTransformationMatrix(false);
            EEPROM::storeDataIntoEEPROM();
        }
        break;
    case 400: // M400 Finish all moves
        Commands::waitUntilEndOfAllMoves();
        break;
    case 401: // M401 Memory position
        Printer::MemoryPosition();
        break;
    case 402: // M402 Go to stored position
        Printer::GoToMemoryPosition(com->hasX(),com->hasY(),com->hasZ(),com->hasE(),(com->hasF() ? com->F : Printer::feedrate));
        break;
   
    default:
        if(Printer::debugErrors())
        {
            Com::printF(Com::tUnknownCommand);
            com->printCommand();
        }
    }
}

/**
  \brief Execute the command stored in com.
*/
void Commands::executeGCode(GCode *com)
{
    if (INCLUDE_DEBUG_COMMUNICATION)
    {
        if(Printer::debugCommunication())
        {
            if(com->hasG() || (com->hasM() && com->M != 111))
            {
                previousMillisCmd = HAL::timeInMilliseconds();
                return;
            }
        }
    }
    if(com->hasG()) processGCode(com);
    else if(com->hasM()) processMCode(com);
    else if(com->hasT())      // Process T code
    {
        Commands::waitUntilEndOfAllMoves();
        Extruder::selectExtruderById(com->T);
		IOExpander.SetActiveExtruder(com->T);
    }
    else
    {
        if(Printer::debugErrors())
        {
            Com::printF(Com::tUnknownCommand);
            com->printCommand();
        }
    }
}

void Commands::emergencyStop()
{
    HAL::resetHardware();
}

void Commands::checkFreeMemory()
{
    int newfree = HAL::getFreeRam();
    if(newfree < lowestRAMValue)
        lowestRAMValue = newfree;
}

void Commands::writeLowestFreeRAM()
{
    if(lowestRAMValueSend>lowestRAMValue)
    {
        lowestRAMValueSend = lowestRAMValue;
        Com::printFLN(Com::tFreeRAM, lowestRAMValue);
    }
}
