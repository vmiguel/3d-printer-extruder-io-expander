// IOExpander.h



#ifndef _IOEXPANDER_h

#define _IOEXPANDER_h



#if defined(ARDUINO) && ARDUINO >= 100

	#include "Arduino.h"

#else

	#include "WProgram.h"

#endif



class IOExpanderClass

{

 protected:





 public:

	 IOExpanderClass();

	static int XeStopHit();

	static int YeStopHit();

	static int ZeStopHit();

	static bool SetPWM(byte ch, byte val);

	static bool SetLEDColor(byte R, byte G, byte B);

	static int GetTemp(byte ex);

	static int GetSetPoint(byte ex);

	static bool SetTemp(byte ex, int temp);
	
	void SetActiveExtruder(uint8_t Ex);
	byte GetActiveExtruder();
private:
	 byte ActiveExtruder;
	



};



extern IOExpanderClass IOExpander;



#endif


