//
//  IOExapander.cpp
//  
//
//  Created by Felipe Neves on 1/28/15.
//
//

#include "Repetier.h"
#include "IOExpander.h"

//
// COnstants:
//
#define SYNC 0x55

//
//  serial cnc states:
//
typedef enum
{
    kIdle,
    kSending,
    kGetReply,
    
}states_t;

//
// custom cncserial data structure:
//
struct _outserial
{
    uint8_t sync;
    uint8_t cmd;
    uint8_t data[2];
    uint8_t chksum;
};

struct _inserial
{
    uint8_t sync;
    uint8_t cmd;
    uint8_t data[2];
    uint8_t chksum;
};

typedef struct _outserial outframe_t;
typedef struct _inserial inframe_t;

//
//Variables:
//

inframe_t input;    //Serial CNC commands
outframe_t output;


//
//  Class APIS:
//
uint8_t SendFrame(uint8_t *output);
//SendFrame:-----------------------------------------------------------------------------------//
uint8_t SendFrame(uint8_t *output)
{
  
    uint8_t *ptr = 0;
    uint8_t i = 0;
    uint8_t chksum = 0;
    outframe_t *out = (outframe_t *)output;
    
    //check null pointer:
    if(out == 0x0000)return -1;
    
#ifdef IOEXPANDER  
    //maps the structres into a byte stream:
    ptr = (uint8_t *)out;


    //Send frame byte to byte:
    for( i = 0; i < sizeof(outframe_t); i++)
    {
        HAL::serial2WriteByte(*ptr);
        ptr++;
    }
    
    
    //Waits until IO expander gets the message:
    HAL::delayMilliseconds(20);
    
    //check for reply 
    if(!HAL::serial2ByteAvailable()) return (0xFF);
 
    
    //get the reply:
    ptr = (uint8_t *)&input;
    
    for( i = 0; i < sizeof(inframe_t); i++)
    {
        *ptr++ = HAL::serial2ReadByte();
    }
    
    //calculates the check sum:
    ptr = (uint8_t*)&input;
    
    //reset checksum count:
    chksum = 0;
    
    //obtains the checksum:
    for ( i = 0; i < sizeof(outframe_t)- 1; i++)
    {
        chksum ^= *ptr++;
    }
#endif    
    //compare:
    if(chksum != input.chksum)return 0xFF;
    
    return 0;
}


//IO:-------------------------------------------------------------------------------------//

IOExpanderClass::IOExpanderClass()
{
    //dummy initialization:
    output.sync = 0x55;
    output.cmd  = 0x00;
    output.data[0] = 0xFF;
    output.data[1] = 0xFF;
    output.chksum = 0x00;
    
    input.sync = 0x55;
    input.cmd  = 0x00;
    input.data[0] = 0xFF;
    input.data[1] = 0xFF;
    input.chksum = 0x00;
	this->ActiveExtruder = 0;
    
}

//IOExpanderSetSoftPWM:-----------------------------------------------------------------------//
bool IOExpanderClass::SetPWM(byte ch, byte val)
{
    uint8_t chksum = 0;
    uint8_t *ptr = 0;
    uint8_t i = 0;
    
    //check channnel valule:
    if(ch > 0x05)return false;
    
    //so, send the request:
    output.cmd = 0x11; //spwm IO command
    output.data[0] = ch;
    output.data[1] = val;
    
    ptr = (uint8_t *)&output;
    
    //obtains the checksum:
    for ( i = 0; i < sizeof(outframe_t)- 1; i++)
    {
        chksum ^= *ptr++;
    }
    
    output.chksum = chksum;
    
    //Send frame via serial.
    if(SendFrame((uint8_t *)&output))return false;
    else return true;
}
//IOExpanderSetHardPWM:-----------------------------------------------------------------------//
bool IOExpanderClass::SetLEDColor(byte R, byte G, byte B)
{

	return (SetPWM(0x02, R) && SetPWM(0x03, G) && SetPWM(0x04, B));

}
//IOExpanderSetHeater:-----------------------------------------------------------------------//
bool IOExpanderClass::SetTemp(uint8_t ex, int temp)
{
    uint8_t chksum = 0;
    uint8_t *ptr = 0;
    uint8_t i = 0;
    
    //check channnel valule:
    if(ex > 0x01)return false;
    
    //so, send the request:
    output.cmd = 0x15; //heater IO command
    output.data[0] = ex;
    output.data[1] = temp;
    
    ptr = (uint8_t *)&output;
    
    //obtains the checksum:
    for ( i = 0; i < sizeof(outframe_t)- 1; i++)
    {
        chksum ^= *ptr++;
    }
    
    output.chksum = chksum;
    
    //Send frame via serial.
    if(SendFrame((uint8_t *)&output))return false;
    else return true;
}
//IOExpanderGetInputs:-----------------------------------------------------------------------//
int IOExpanderClass::XeStopHit()
{
    uint8_t chksum = 0;
    uint8_t *ptr = 0;
    uint8_t i = 0;
    
    //check channnel valule:
    //if(channel > 0x01)return -1;
    
    //so, send the request:
    output.cmd = 0x13; //heater IO command
    output.data[0] = 0xFF;
    output.data[1] = 0xFF;
    
    ptr = (uint8_t *)&output;
    
    //obtains the checksum:
    for ( i = 0; i < sizeof(outframe_t)- 1; i++)
    {
        chksum ^= *ptr++;
    }
    
    output.chksum = chksum;
    
    //Send frame via serial.
    SendFrame((uint8_t *)&output);
    
   return((int) (input.data[1] & 0x01));
}    
//IOExpanderGetInputs:-----------------------------------------------------------------------//
int IOExpanderClass::YeStopHit()
{
    uint8_t chksum = 0;
    uint8_t *ptr = 0;
    uint8_t i = 0;
    
    //check channnel valule:
    //if(channel > 0x01)return -1;
    
    //so, send the request:
    output.cmd = 0x13; //heater IO command
    output.data[0] = 0xFF;
    output.data[1] = 0xFF;
    
    ptr = (uint8_t *)&output;
    
    //obtains the checksum:
    for ( i = 0; i < sizeof(outframe_t)- 1; i++)
    {
        chksum ^= *ptr++;
    }
    
    output.chksum = chksum;
    
    //Send frame via serial.
    SendFrame((uint8_t *)&output);
    
    return( (int)((input.data[1] >> 1) & 0x01));
    
}
//IOExpanderGetInputs:-----------------------------------------------------------------------//
int IOExpanderClass::ZeStopHit()
{
    uint8_t chksum = 0;
    uint8_t *ptr = 0;
    uint8_t i = 0;
    
    //check channnel valule:
    //if(channel > 0x01)return -1;
    
    //so, send the request:
    output.cmd = 0x13; //heater IO command
    output.data[0] = 0xFF;
    output.data[1] = 0xFF;
    
    ptr = (uint8_t *)&output;
    
    //obtains the checksum:
    for ( i = 0; i < sizeof(outframe_t)- 1; i++)
    {
        chksum ^= *ptr++;
    }
    
    output.chksum = chksum;
    
    //Send frame via serial.
    SendFrame((uint8_t *)&output);
    
   return((int) ((input.data[1] >> 2) & 0x01));
   
   
}

//IOExpanderGetInputs:-----------------------------------------------------------------------//
int IOExpanderClass::GetTemp(uint8_t ex)
{
	//if (ex == 0)
	//	return 11;
	//if (ex == 1)
	//	return 22;
    uint8_t chksum = 0;
    uint8_t *ptr = 0;
    uint8_t i = 0;
    
    //check channnel valule:
    if(ex > 0x03)return 0;
    
    //so, send the request:
    output.cmd = 0x14; //heater IO command
    output.data[0] = ex;
    output.data[1] = 0xFF;
    
    ptr = (uint8_t *)&output;
    
    //obtains the checksum:
    for ( i = 0; i < sizeof(outframe_t)- 1; i++)
    {
        chksum ^= *ptr++;
    }
    
    output.chksum = chksum;
    
    //Send frame via serial.
    if(SendFrame((uint8_t *)&output))return 0;
    
    else
    {
        return( input.data[1]);
    }
}



//--------------------------------------------------------------------------------------------------//
int IOExpanderClass::GetSetPoint(byte ex)
{
	//if (ex == 0)
	//	return 110;
	//if (ex == 1)
	//	return 220;
    uint8_t chksum = 0;
    uint8_t *ptr = 0;
    uint8_t i = 0;
    
    //check channnel valule:
    if(ex > 0x02)return 0;
    
    //so, send the request:
    output.cmd = 0x16; //get heater IO command
    output.data[0] = ex;
    output.data[1] = 0xFF;
    
    ptr = (uint8_t *)&output;
    
    //obtains the checksum:
    for ( i = 0; i < sizeof(outframe_t)- 1; i++)
    {
        chksum ^= *ptr++;
    }
    
    output.chksum = chksum;
    
    //Send frame via serial.
    if(SendFrame((uint8_t *)&output))return 0;
    else
    {
        return( input.data[1]);
    }

}
//--------------------------------------------------------------------------------------------------//
void IOExpanderClass::SetActiveExtruder(uint8_t Ex)
{
	// aqui entra a rotina de troca de extrusor.
	this->ActiveExtruder = (byte)Ex;
}
byte IOExpanderClass::GetActiveExtruder()
{
	return this->ActiveExtruder;
}
IOExpanderClass IOExpander;
